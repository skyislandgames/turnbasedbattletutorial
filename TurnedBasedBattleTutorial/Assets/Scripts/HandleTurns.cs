/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/

using System;
using UnityEngine;

[Serializable]
public class HandleTurns  {

    public string Attacker; //name of attacker
    public string Type;
    public GameObject AttackersGameObject; //who attacks
    public GameObject AttackersTarget; //who is going to be attacked

    //Which Attack is performed
    public BaseAttack ChosenAttack;	
}
