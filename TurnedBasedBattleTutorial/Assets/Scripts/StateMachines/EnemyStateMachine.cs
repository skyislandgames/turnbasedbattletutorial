/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateMachine : MonoBehaviour {

    #region Variables
    private BattleStateMachine BSM;

    public BaseEnemy enemy;

    public enum TurnState
    {
        PROCESSING,
        CHOOSEACTION,
        WAITING,
        ACTION,
        DEAD
    }

    public TurnState currentState;

    //for health bar (progress bar)
    public float cur_cooldown = 0f;
    public float max_cooldown = 7f;
    //this game object
    private Vector3 startPosition;
    public GameObject Selector;
    //time for action vars
    private bool actionStarted = false;
    public GameObject HeroToAttack;
    public float animSpeed = 5f;

    //alive
    private bool alive = true;

    #endregion

    #region Unity Methods

    void Start () {
        currentState = TurnState.PROCESSING;
        BSM = GameObject.Find("BattleManager").GetComponent<BattleStateMachine>();
        startPosition = transform.position;
        Selector.SetActive(false);
    }
	
	
	void Update () {
        switch (currentState)
        {
            case (TurnState.PROCESSING):
                UpgradeProgressBar();
                break;
            case (TurnState.CHOOSEACTION):
                ChooseAction();
                currentState = TurnState.WAITING;
                break;
            case (TurnState.WAITING):
                //idle
                break;
            case (TurnState.ACTION):
                StartCoroutine(TimeForAction());
                break;
            case (TurnState.DEAD):
                if (!alive)
                {
                    return;
                }
                else
                {
                    //Change Tag
                    this.gameObject.tag = "DeadEnemy";
                    //not attackable by enemy
                    BSM.EnemysInBattle.Remove(this.gameObject);
                  
                    //deactivate selector if on
                    Selector.SetActive(false);

                    //remove from perform list
                    if (BSM.EnemysInBattle.Count > 0)
                    {
                        for (int i = 0; i < BSM.PerformList.Count; i++)
                        {
                            if (i != 0)
                            {
                                if (BSM.PerformList[i].AttackersGameObject == this.gameObject)
                                {
                                    BSM.PerformList.Remove(BSM.PerformList[i]);
                                }

                                if (BSM.PerformList[i].AttackersTarget == this.gameObject)
                                {
                                    BSM.PerformList[i].AttackersTarget = BSM.EnemysInBattle[Random.Range(0, BSM.EnemysInBattle.Count)];
                                }
                            }
                        }
                    }
                   
                    //change color/ play animation
                    this.gameObject.GetComponent<MeshRenderer>().material.color = new Color32(105, 105, 105, 255);
                    //reset hero Input
                    BSM.EnemyButtons();
                    BSM.battleState = BattleStateMachine.PerformAction.CHECKALIVE;
                    alive = false;
                }
                break;
            default:
                break;
        }
    }

    void UpgradeProgressBar()
    {
        cur_cooldown = cur_cooldown + Time.deltaTime;
        if (cur_cooldown >= max_cooldown)
        {
            if (BSM.HerosInBattle.Count > 0)
            {
                currentState = TurnState.CHOOSEACTION;
            }
            else
            {
                currentState = TurnState.WAITING;
            }
        }
    }

    void ChooseAction()
    {
        HandleTurns attackDetails = new HandleTurns();
        attackDetails.Attacker = enemy.theName;
        attackDetails.Type = "Enemy";
        attackDetails.AttackersGameObject = this.gameObject;
        attackDetails.AttackersTarget = BSM.HerosInBattle[Random.Range(0, BSM.HerosInBattle.Count)]; //sets AI to randomly attack anyone TODO:

        int num = Random.Range(0, enemy.attacks.Count);//random choose index of attack in attacks list
        attackDetails.ChosenAttack = enemy.attacks[num];
        Debug.Log(this.gameObject.name + " has performed attack: " + attackDetails.ChosenAttack.attackName + " and did " + attackDetails.ChosenAttack.attackDamage + " damage!");
        BSM.CollectActions(attackDetails);
    }


    private IEnumerator TimeForAction()
    {
        if (actionStarted){
            yield break;
        }

        actionStarted = true;

        //animate the enemy near the hero to attack
        Vector3 heroPosistion = new Vector3(HeroToAttack.transform.position.x-1.5f, HeroToAttack.transform.position.y, HeroToAttack.transform.position.z);
        while (MoveTowardsEnemy(heroPosistion))
        {
            yield return null;
        }
        //wait a bit
        yield return new WaitForSeconds(0.5f);
        //do damage
        DoDamage();
        //animate back to startPos
        Vector3 firstPosition = startPosition;
        while (MoveTowardsStart(firstPosition))
        {
            yield return null;
        }
        //remove the perfomer formt he list in the BSM
        BSM.PerformList.RemoveAt(0);
        //Reset BSM -> wait
        BSM.battleState = BattleStateMachine.PerformAction.WAIT;
        //end coroutine
        actionStarted = false;
        //reset this enemy state

        cur_cooldown = 0f;
        currentState = TurnState.PROCESSING;

    }


    //combine these later
    private bool MoveTowardsEnemy (Vector3 target)
    {
        return target != (transform.position = Vector3.MoveTowards(transform.position,target, animSpeed * Time.deltaTime));
    }

    private bool MoveTowardsStart(Vector3 target)
    {
        return target != (transform.position = Vector3.MoveTowards(transform.position, target, animSpeed * Time.deltaTime));
    }

    public void TakeDamage(float getDamageAmount)
    {
        enemy.curHP -= getDamageAmount;
        if (enemy.curHP <= 0)
        {
            enemy.curHP = 0;
            currentState = TurnState.DEAD;
        }
    }

    void DoDamage()
    {
        float calc_Damage = enemy.curATK + BSM.PerformList[0].ChosenAttack.attackDamage;
        HeroToAttack.GetComponent<HeroStateMachine>().TakeDamage(calc_Damage);
    }
    #endregion

}
