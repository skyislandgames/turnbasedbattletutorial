/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleStateMachine : MonoBehaviour
{

    #region Variables
    public enum PerformAction
    {
        WAIT,
        TAKEACTION,
        PERFORMACTION,
        CHECKALIVE,
        WIN,
        LOSE
    }

    public PerformAction battleState;

    public List<HandleTurns> PerformList = new List<HandleTurns>();

    public List<GameObject> HerosInBattle = new List<GameObject>();
    public List<GameObject> EnemysInBattle = new List<GameObject>();

    public enum HeroGUI
    {
        ACTIVATE,
        WAITING,
        INPUT1,
        INPUT2,
        DONE
    }

    public HeroGUI HeroInput;

    public List<GameObject> HerosToManage = new List<GameObject>();
    private HandleTurns HerosChoice;

    public GameObject enemyButton;
    public Transform Spacer;

    public GameObject ActionPanel;
    public GameObject EnemySelectPanel;
    public GameObject MagicPanel;

    public Transform ActionSpacer;
    public Transform MagicSpacer;
    public GameObject ActionBtn;
    public GameObject MagicBtn;

    public List<GameObject> atkBtns = new List<GameObject>();

    private List<GameObject> enemybtns = new List<GameObject>();

    //spawn Points
    public List<Transform> spawnPoints = new List<Transform>();

    #endregion

    #region Unity Methods

    private void Awake()
    {
        for (int i = 0; i < GameManager.instance.enemyAmount; i++)
        {
            GameObject NewEnemy = Instantiate(GameManager.instance.enemysToBattle[i], spawnPoints[i].position, Quaternion.identity) as GameObject;
            NewEnemy.name = NewEnemy.GetComponent<EnemyStateMachine>().enemy.theName + "_" + (i + 1);
            NewEnemy.GetComponent<EnemyStateMachine>().enemy.theName = NewEnemy.name;
            EnemysInBattle.Add(NewEnemy);
        }
    }


    void Start()
    {
        battleState = PerformAction.WAIT;
        //EnemysInBattle.AddRange(GameObject.FindGameObjectsWithTag("Enemy"));
        HerosInBattle.AddRange(GameObject.FindGameObjectsWithTag("Hero"));
        HeroInput = HeroGUI.ACTIVATE;

        ActionPanel.SetActive(false);
        EnemySelectPanel.SetActive(false);
        MagicPanel.SetActive(false);

        EnemyButtons();
    }


    void Update()
    {
        switch (battleState)
        {
            case (PerformAction.WAIT):
                if (PerformList.Count > 0)
                {
                    battleState = PerformAction.TAKEACTION;
                }
                break;
            case (PerformAction.TAKEACTION):
                GameObject performer = GameObject.Find(PerformList[0].Attacker);
                if (PerformList[0].Type == "Enemy")
                {
                    EnemyStateMachine ESM = performer.GetComponent<EnemyStateMachine>();
                    for (int i = 0; i < HerosInBattle.Count; i++)
                    {
                        if (PerformList[0].AttackersTarget == HerosInBattle[i])
                        {

                            ESM.HeroToAttack = PerformList[0].AttackersTarget;
                            ESM.currentState = EnemyStateMachine.TurnState.ACTION;
                            break;
                        }
                        else
                        {
                            PerformList[0].AttackersTarget = HerosInBattle[Random.Range(0, HerosInBattle.Count)];
                            ESM.HeroToAttack = PerformList[0].AttackersTarget;
                            ESM.currentState = EnemyStateMachine.TurnState.ACTION;
                        }
                    }
                }
                if (PerformList[0].Type == "Hero")
                {
                    HeroStateMachine HSM = performer.GetComponent<HeroStateMachine>();
                    HSM.EnemyToAttack = PerformList[0].AttackersTarget;
                    HSM.currentState = HeroStateMachine.TurnState.ACTION;
                }
                battleState = PerformAction.PERFORMACTION;
                break;
            case (PerformAction.PERFORMACTION):
                //Space for being Idle
                break;
            case (PerformAction.CHECKALIVE):
                if (HerosInBattle.Count < 1)
                {
                    battleState = PerformAction.LOSE;
                }
                else if (EnemysInBattle.Count < 1)
                {
                    battleState = PerformAction.WIN;
                }
                else
                {
                    ClearAttackPanel();
                    HeroInput = HeroGUI.ACTIVATE;
                }
                break;
            case (PerformAction.WIN):
                {
                    for (int i = 0; i < HerosInBattle.Count; i++)
                    {
                        HerosInBattle[i].GetComponent<HeroStateMachine>().currentState = HeroStateMachine.TurnState.WAITING;
                    }
                    GameManager.instance.LoadSceneAfterBattle();
                    GameManager.instance.gameState = GameManager.GameStates.WORLD_STATE;
                    GameManager.instance.enemysToBattle.Clear();
                }
                break;
            case (PerformAction.LOSE):
                {
                    for (int i = 0; i < EnemysInBattle.Count; i++)
                    {
                        EnemysInBattle[i].GetComponent<EnemyStateMachine>().currentState = EnemyStateMachine.TurnState.WAITING;
                    }
                }
                break;
            default:
                break;
        }

        switch (HeroInput)
        {
            case (HeroGUI.ACTIVATE):
                if (HerosToManage.Count > 0)
                {
                    HerosToManage[0].transform.Find("Selector").gameObject.SetActive(true);
                    HerosChoice = new HandleTurns();

                    ActionPanel.SetActive(true);
                    CreateAttackButtons();

                    HeroInput = HeroGUI.WAITING;
                }
                break;
            case (HeroGUI.WAITING):
                break;
            case (HeroGUI.INPUT1):
                break;
            case (HeroGUI.INPUT2):
                break;
            case (HeroGUI.DONE):
                HeroInputDone();
                break;
            default:
                break;
        }
    }
    public void CollectActions(HandleTurns input)
    {
        PerformList.Add(input);
    }

    public void EnemyButtons()
    {
        //clear buttons
        foreach (GameObject enemybtn in enemybtns)
        {
            Destroy(enemybtn);
        }
        enemybtns.Clear();

        //create buttons
        foreach (GameObject enemy in EnemysInBattle)
        {
            GameObject newButton = Instantiate(enemyButton) as GameObject;
            EnemySelectButton button = newButton.GetComponent<EnemySelectButton>();

            EnemyStateMachine cur_enemy = enemy.GetComponent<EnemyStateMachine>();

            Text buttonText = newButton.transform.Find("Target").gameObject.GetComponent<Text>();
            buttonText.text = cur_enemy.enemy.theName;

            button.EnemyPrefab = enemy;

            newButton.transform.SetParent(Spacer, false);
            enemybtns.Add(newButton);
        }
    }

    void CreateAttackButtons()
    {
        GameObject AttackButton = Instantiate(ActionBtn) as GameObject;
        Text AttactButtonText = AttackButton.transform.Find("Attack").gameObject.GetComponent<Text>();
        AttactButtonText.text = "Attack";
        AttackButton.GetComponent<Button>().onClick.AddListener(() => Input1());
        AttackButton.transform.SetParent(ActionSpacer, false);
        atkBtns.Add(AttackButton);

        GameObject MagicButton = Instantiate(ActionBtn) as GameObject;

        //make magic buttons if hero can use
        if (HerosToManage[0].GetComponent<HeroStateMachine>().hero.MagicAttacks.Count > 0)
        {
            Text MagicButtonText = MagicButton.transform.Find("Attack").gameObject.GetComponent<Text>();
            MagicButtonText.text = "Magic";
            MagicButton.GetComponent<Button>().onClick.AddListener(() => Input3());
            MagicButton.transform.SetParent(ActionSpacer, false);
            atkBtns.Add(MagicButton);

            foreach (BaseAttack magicAtk in HerosToManage[0].GetComponent<HeroStateMachine>().hero.MagicAttacks)
            {
                GameObject SpellButton = Instantiate(MagicBtn) as GameObject;
                Text MagicText = SpellButton.transform.Find("Attack").gameObject.GetComponent<Text>();
                MagicText.text = magicAtk.attackName;
                MagicAttack mb = SpellButton.GetComponent<MagicAttack>();
                mb.magicAttackToPerform = magicAtk;
                SpellButton.transform.SetParent(MagicSpacer, false);

                atkBtns.Add(SpellButton);

            }
        }
        else
        {
            MagicButton.GetComponent<Button>().interactable = false;
        }
    }


    public void Input1()//Attack
    {
        HerosChoice.Attacker = HerosToManage[0].name;
        HerosChoice.AttackersGameObject = HerosToManage[0];
        HerosChoice.Type = "Hero";
        HerosChoice.ChosenAttack = HerosToManage[0].GetComponent<HeroStateMachine>().hero.attacks[0];

        ActionPanel.SetActive(false);
        EnemySelectPanel.SetActive(true);
    }

    public void Input2(GameObject chosenEnemy)//Enemy Selection
    {
        HerosChoice.AttackersTarget = chosenEnemy;
        HeroInput = HeroGUI.DONE;
    }





    public void Input4(BaseAttack ChosenMagic) //choose magic attack
    {
        HerosChoice.Attacker = HerosToManage[0].name;
        HerosChoice.AttackersGameObject = HerosToManage[0];
        HerosChoice.Type = "Hero";

        HerosChoice.ChosenAttack = ChosenMagic;
        MagicPanel.SetActive(false);
        EnemySelectPanel.SetActive(true);
    }

    public void Input3() //switch to magic attack
    {
        ActionPanel.SetActive(false);
        MagicPanel.SetActive(true);
    }

    void HeroInputDone()
    {
        PerformList.Add(HerosChoice);
        //clean attack panel
        ClearAttackPanel();

        HerosToManage[0].transform.Find("Selector").gameObject.SetActive(false);
        HerosToManage.RemoveAt(0);
        HeroInput = HeroGUI.ACTIVATE;
    }

    void ClearAttackPanel()
    {
        EnemySelectPanel.SetActive(false);
        ActionPanel.SetActive(false);
        MagicPanel.SetActive(false);

        foreach (GameObject atkBtn in atkBtns)
        {
            Destroy(atkBtn);
        }
        atkBtns.Clear();
    }
    #endregion

}
