/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using UnityEngine;

public class ItemLoader : MonoBehaviour
{

    public const string path = "items";

    void Start()
    {
        ItemContainer ic = ItemContainer.Load(path);

        foreach (Item item in ic.items)
        {
            print(item.name);
        }
    }




}
