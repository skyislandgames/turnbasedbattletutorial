/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using UnityEngine;
using System.Collections.Generic;

public class RegionData : MonoBehaviour {



    public int maxAmountEnemys = 4;
    public List<GameObject> possibleEnemys = new List<GameObject>();
    public string BattleScene;

}
