/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using UnityEngine;

public class CameraController : MonoBehaviour
{
  
    public Transform target;
    public float lookSmooth = 0.9f;
    public Vector3 offsetFromTarget = new Vector3(0, 6, -8);
    public float xTilt = 10;

    Vector3 destination = Vector3.zero;
    HeroMovement charController;
    float rotateVel = 0;


    private void Start()
    {
        target = GameObject.Find("HeroCharater").gameObject.transform;
        SetCameraTarget(target);
    }

    public void SetCameraTarget(Transform t)
    {
        target = t;

        if(target != null)
        {
            if (target.GetComponent<HeroMovement>())
            {
                charController = target.GetComponent<HeroMovement>();
            }
            else
            {
                Debug.LogError("Missing heromovement on target");
            }
        }
        else
        {
            Debug.LogError("Missing target on camera");
        }
    }

    private void LateUpdate()
    {
        //moving
        MoveToTarget();
        //rotating
        LookAtTarget();
    }

    void MoveToTarget()
    {
        destination = charController.TargetRotations * offsetFromTarget;
        destination += target.position;
        transform.position = destination;
    }

    void LookAtTarget()
    {
        float eulerYAngle = Mathf.SmoothDampAngle(transform.eulerAngles.y, target.eulerAngles.y, ref rotateVel,lookSmooth);
        transform.rotation = Quaternion.Euler(transform.eulerAngles.x, eulerYAngle, 0);
    }
   
}
