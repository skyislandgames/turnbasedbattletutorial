/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;


    public RegionData curRegion;

    public GameObject heroCharater;

    public Vector3 nextHeroPosistion;
    public Vector3 lastHeroPosistion;

    public string sceneToLoad;
    public string lastScene;

    public bool isWalking = false;
    public bool canGetEncounter = false;
    public bool gotAttacked = false;


    public enum GameStates
    {
        WORLD_STATE,
        TOWN_STATE,
        BATTLE_STATE,
        MENU_STATE,
        IDLE
    }

    public List<GameObject> enemysToBattle = new List<GameObject>();

    public int enemyAmount;

    public string nextSpawnPoint;




    public GameStates gameState;

    private void Awake()
    {
        //Check if instance exists
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);


        if (SceneManager.GetActiveScene().buildIndex != 0)
        {
            if (!GameObject.Find("HeroCharater"))
            {
                GameObject Hero = Instantiate(heroCharater, Vector3.zero + new Vector3(0,0.5f,0), Quaternion.identity) as GameObject;
                Hero.name = "HeroCharater";
            }
        }
        else
        {
            StartCoroutine(Countdown());
        }
    }

    private IEnumerator Countdown()
    {
        yield return new WaitForSeconds(4);
        float fadeTime = this.GetComponent<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
        SceneManager.LoadScene(1);

    }

    private void Update()
    {
        switch (gameState)
        {
            case (GameStates.WORLD_STATE):
                if (isWalking)
                {
                    RandomEnconter();
                }
                if (gotAttacked)
                {
                    gameState = GameStates.BATTLE_STATE;
                }
                break;
            case (GameStates.TOWN_STATE):
                break;
            case (GameStates.BATTLE_STATE):
                //load battle scene
                StartBattle();
                //Go to idle
                gameState = GameStates.IDLE;
                break;
            case (GameStates.MENU_STATE):
                break;
            case (GameStates.IDLE):
                break;
            default:
                break;
        }
    }

    //public void LoadNextScene()
    //{
    //    SceneManager.LoadScene(sceneToLoad);
    //}

    public void LoadSceneAfterBattle()
    {
        SceneManager.LoadScene(lastScene);
    }

    public void LoadNextScene()
    {
        //GameObject.Find("HeroCharater").GetComponent<HeroMovement>().disableMovement = true;
        StartCoroutine(ILoadNextScene(sceneToLoad));
        //GameObject.Find("HeroCharater").GetComponent<HeroMovement>().disableMovement = false;
    }
    //public void LoadSceneAfterBattle()
    //{
    //    StartCoroutine(ILoadNextScene(lastScene));
    //}
    public IEnumerator ILoadNextScene(string sceneToLoad)
    {
        
        float fadeTime = this.GetComponent<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
        SceneManager.LoadScene(sceneToLoad);
        
    }


    void RandomEnconter()
    {
        if (isWalking && canGetEncounter)
        {
            if(Random.Range(0,1000)< 10)
            {
                //Debug.Log("ATTACKED!!!!!!");
                gotAttacked = true;
            }
        }
    }

    private void StartBattle()
    {
        //amount of enemys
        enemyAmount = Random.Range(1, curRegion.maxAmountEnemys + 1);
        //which enemys
        for (int i = 0; i < enemyAmount; i++)
        {
            enemysToBattle.Add(curRegion.possibleEnemys[Random.Range(0, curRegion.possibleEnemys.Count)]);

        }

        //hero
        lastHeroPosistion = GameObject.Find("HeroCharater").gameObject.transform.position;
        nextHeroPosistion = lastHeroPosistion;
        lastScene = SceneManager.GetActiveScene().name;
        //reset hero
        isWalking = false;
        gotAttacked = false;
        canGetEncounter = false;
        //load level
        SceneManager.LoadScene(curRegion.BattleScene);

    }
}
