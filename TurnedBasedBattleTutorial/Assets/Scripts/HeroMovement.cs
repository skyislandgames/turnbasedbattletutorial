/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using UnityEngine;

public class HeroMovement : MonoBehaviour
{

    [System.Serializable]
    public class MoveSettings
    {
        public float forwardVel = 12;
        public float rotateVel = 100;
        public float jumpVel = 25;
        public float distToGround = 0.5f;
        public LayerMask ground;
    }

    [System.Serializable]
    public class PhysicsSettings
    {
        public float downAccel = 0.75f;
    }

    [System.Serializable]
    public class InputSettings
    {
        public float inputDelay = 0.1f;
        public string FORWARD_AXIS = "Vertical";
        public string TURN_AXIS = "Horizontal";
        public string JUMP_AXIS = "Jump";

    }

    public MoveSettings moveSettings = new MoveSettings();
    public PhysicsSettings physicsSettings = new PhysicsSettings();
    public InputSettings inputSettings = new InputSettings();

    Vector3 curPos, lastPos;
    Vector3 velocity = Vector3.zero;
    private Quaternion targetRotation;
    private Rigidbody rBody;
    private float forwardInput, turnInput, jumpInput;

    public Quaternion TargetRotations
    {
        get { return targetRotation; }
    }

    bool Grounded()
    {
        Debug.Log(Physics.Raycast(transform.position, Vector3.down, moveSettings.distToGround, moveSettings.ground));
        return Physics.Raycast(transform.position, Vector3.down, moveSettings.distToGround, moveSettings.ground);
    }

    void Start()
    {
        if (GameManager.instance.nextSpawnPoint != "")
        {
            GameObject spawnPoint = GameObject.Find(GameManager.instance.nextSpawnPoint);
            transform.position = spawnPoint.transform.position;

            GameManager.instance.nextSpawnPoint = "";
        }
        else if (GameManager.instance.lastHeroPosistion != Vector3.zero)
        {
            transform.position = GameManager.instance.lastHeroPosistion;
            GameManager.instance.lastHeroPosistion = Vector3.zero;
        }

        targetRotation = transform.rotation;
        if (GetComponent<Rigidbody>())
        {
            rBody = GetComponent<Rigidbody>();
        }
        else
        {
            Debug.LogError("Character missing rigidbody");
        }

        forwardInput = turnInput = jumpInput = 0;

    }

    void GetInput()
    {
        forwardInput = Input.GetAxis(inputSettings.FORWARD_AXIS);
        turnInput = Input.GetAxis(inputSettings.TURN_AXIS);
        jumpInput = Input.GetAxisRaw(inputSettings.JUMP_AXIS);
    }

    private void Update()
    {
        GetInput();
        Turn();
    }

    void FixedUpdate()
    {

        Run();

        Jump();

        rBody.velocity = transform.TransformDirection(velocity);

        curPos = transform.position;
        if (curPos == lastPos)
        {
            GameManager.instance.isWalking = false;
        }
        else
        {
            GameManager.instance.isWalking = true;
        }

        lastPos = curPos;

    }


    void Run()
    {
        if (Mathf.Abs(forwardInput) > inputSettings.inputDelay)
        {
            //move
            velocity.z = moveSettings.forwardVel * forwardInput;
        }
        else
        {
            //zero Velocity
            velocity.z = 0;
        }
    }

    void Turn()
    {
        if (Mathf.Abs(turnInput) > inputSettings.inputDelay)
        {
            targetRotation *= Quaternion.AngleAxis(moveSettings.rotateVel * turnInput * Time.deltaTime, Vector3.up);
        }
        transform.rotation = targetRotation;
    }

    void Jump()
    {
        
        if (jumpInput > 0 && Grounded())
        {
            //jump
            velocity.y = moveSettings.jumpVel;

        }
        else if (jumpInput == 0 && Grounded())
        {
            //zero out velecity y
            velocity.y = 0;
        }
        else
        {
            //decrease velecity y
            velocity.y -= physicsSettings.downAccel;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other == null)
        {
            return;
        }


        if (other.tag == "teleporter")
        {
            CollisionHandler colH = other.gameObject.GetComponent<CollisionHandler>();
            //GameManager.instance.nextHeroPosistion = colH.spawnPoint.transform.position;
            GameManager.instance.nextSpawnPoint = colH.spawnPointName;
            GameManager.instance.sceneToLoad = colH.sceneToLoad;
            GameManager.instance.LoadNextScene();
        }


        if (other.tag == "EncounterZone")
        {
            RegionData region = other.gameObject.GetComponent<RegionData>();
            GameManager.instance.curRegion = region;
        }

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "EncounterZone")
        {

            GameManager.instance.canGetEncounter = true;

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "EncounterZone")
        {
            GameManager.instance.canGetEncounter = false;
        }
    }


}
