/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using UnityEngine;

public class EnemySelectButton : MonoBehaviour
{
    #region Variables
    public GameObject EnemyPrefab;

    #endregion

    #region Unity Methods
    public void SelectEnemy()
    {
        GameObject.Find("BattleManager").GetComponent<BattleStateMachine>().Input2(EnemyPrefab); //save input of enemy prefab

    }

    public void HideSelector()
    {
        EnemyPrefab.transform.Find("Selector").gameObject.SetActive(false);
    }
    public void ShowSelector()
    {
        EnemyPrefab.transform.Find("Selector").gameObject.SetActive(true);
    }
    #endregion
}
