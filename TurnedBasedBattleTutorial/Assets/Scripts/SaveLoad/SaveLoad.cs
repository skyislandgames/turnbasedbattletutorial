/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/

using System;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Text;
using System.Security.Cryptography;

public class SaveLoad : MonoBehaviour {

    private int hasSaved = 0;
    private HeroMovement HSM;

    void Start()
    {
        HSM = GameObject.Find("HeroCharater").gameObject.GetComponent<HeroMovement>();
    }

    public void Save()
    {
        ////PlayerPrefs
        //PlayerPrefs.SetInt("hasSaved", hasSaved);
        //PlayerPrefs.SetFloat("moveSpeed", HSM.moveSpeed);

        BinaryFormatter binary = new BinaryFormatter();
        FileStream fStream = File.Create(Application.persistentDataPath + "/saveFile.sig");
        Stream cs = Crypto.CreateCryptoStreamAESWrite("SavedData", fStream);

        SaveManager saver = new SaveManager();
       // saver._moveSpeed = HSM.moveSpeed;
        //All Other..

        binary.Serialize(cs, saver);
        cs.Close();
        fStream.Close();
      
    }

    public void Load()
    {
        ////PlayerPrefs
        //if (PlayerPrefs.HasKey("hasSaved"))
        //{
        //    HSM.moveSpeed = PlayerPrefs.GetFloat("moveSpeed");
        //}

        if (File.Exists(Application.persistentDataPath + "/saveFile.sig"))
        {
            BinaryFormatter binary = new BinaryFormatter();
            FileStream fStream = File.Open(Application.persistentDataPath + "/saveFile.sig",FileMode.Open);
            Stream cs = Crypto.CreateCryptoStreamAESRead("SavedData", fStream);
            SaveManager saver = (SaveManager)binary.Deserialize(cs);
            fStream.Close();

           // HSM.moveSpeed = saver._moveSpeed;
        }

   
        

    }

    public void Delete()
    {
        ////PlayerPrefs
        //PlayerPrefs.DeleteAll();

        if (File.Exists(Application.persistentDataPath + "/saveFile.sig"))
        {
             File.Delete(Application.persistentDataPath + "/saveFile.sig");

        }
    }
}


[Serializable]
class SaveManager
{
    public float _moveSpeed;
    //Add More...
}


#region AES Encryption
public class Crypto
{
    private static byte[] _salt = Encoding.ASCII.GetBytes("42kb$2fs$@#GE$^%gdhf;!M807c5o666");

    public static Stream CreateCryptoStreamAESWrite(string sharedSecret, Stream stream)
    {
        if (string.IsNullOrEmpty(sharedSecret))
            throw new ArgumentNullException("sharedSecret");

        // generate the key from the shared secret and the salt
        Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, _salt);

        // Create a RijndaelManaged object
        RijndaelManaged aesAlg = new RijndaelManaged();
        aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

        // Create a decryptor to perform the stream transform.
        ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

        // prepend the IV
        stream.Write(BitConverter.GetBytes(aesAlg.IV.Length), 0, sizeof(int));
        stream.Write(aesAlg.IV, 0, aesAlg.IV.Length);

        CryptoStream csEncrypt = new CryptoStream(stream, encryptor, CryptoStreamMode.Write);
        return csEncrypt;
    }

    public static Stream CreateCryptoStreamAESRead(string sharedSecret, Stream stream)
    {
        if (string.IsNullOrEmpty(sharedSecret))
            throw new ArgumentNullException("sharedSecret");

        // generate the key from the shared secret and the salt
        Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, _salt);

        // Create a RijndaelManaged object
        // with the specified key and IV.
        RijndaelManaged aesAlg = new RijndaelManaged();
        aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

        // Get the initialization vector from the encrypted stream
        aesAlg.IV = ReadByteArray(stream);

        // Create a decrytor to perform the stream transform.
        ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
        CryptoStream csDecrypt = new CryptoStream(stream, decryptor, CryptoStreamMode.Read);
        return csDecrypt;
    }

    private static byte[] ReadByteArray(Stream s)
    {
        byte[] rawLength = new byte[sizeof(int)];
        if (s.Read(rawLength, 0, rawLength.Length) != rawLength.Length)
            throw new SystemException("Stream did not contain properly formatted byte array");

        byte[] buffer = new byte[BitConverter.ToInt32(rawLength, 0)];
        if (s.Read(buffer, 0, buffer.Length) != buffer.Length)
            throw new SystemException("Did not read byte array properly");

        return buffer;
    }
}

#endregion