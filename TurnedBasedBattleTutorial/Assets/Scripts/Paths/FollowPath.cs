/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using UnityEngine;

public class FollowPath : MonoBehaviour {

    public EditorPaths PathToFollow;

    public int CurrentWayPointID;
    public float speed;

    private float reachDistance = 1.0f;
    public float rotationSpeed = 5.0f;
    public string pathName;

    private Vector3 last_position;
    private Vector3 current_position;

    private void Start()
    {
        //PathToFollow = GameObject.Find(pathName).GetComponent<EditorPaths>();
        last_position = transform.position;
    }

    private void Update()
    {
        float distance = Vector3.Distance(PathToFollow.path_objs[CurrentWayPointID].position,transform.position);
        transform.position = Vector3.MoveTowards(transform.position, PathToFollow.path_objs[CurrentWayPointID].position, speed * Time.deltaTime);

        var rotation = Quaternion.LookRotation(PathToFollow.path_objs[CurrentWayPointID].position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);

        if(distance <= reachDistance)
        {
            CurrentWayPointID++;
        }

        if(CurrentWayPointID >= PathToFollow.path_objs.Count)
        {
            CurrentWayPointID = 0;
        }
    }

}
