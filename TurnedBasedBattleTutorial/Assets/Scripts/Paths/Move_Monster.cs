/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using System.Collections;
using UnityEngine;

public class Move_Monster : MonoBehaviour {

    public enum MovementStates
    {
        IDLE,
        WALKING,
        SLEEPING
    }

    public MovementStates movementState;

    public float speed = 2.0f;
    public float rotateSpeed = 2.0f;

    public GameObject wayPoint;
    private GameObject WP;

    public float min_distance = -2.0f;
    public float max_distance = 2.0f;
    private bool wayP = false;


    private Animator anim;

    private void Awake()
    {
        //anim = GetComponent<Animator>();
    }

    private void Start()
    {
        StartCoroutine(ChooseAction());
    }

    private void Update()
    {
        switch (movementState)
        {
            case (MovementStates.WALKING):
                //anim.SetBool("is_moving", true);
                CreateWayPoint();
                break;
            case (MovementStates.IDLE):
                //anim.SetBool("is_moving", false);
                break;
            case (MovementStates.SLEEPING):
                break;
            default:
                break;
        }

        if (wayP)
        {
            Move();
        }
       
    }

    void CreateWayPoint()
    {
        if (!wayP)
        {
            float distance_x = transform.position.x + Random.Range(min_distance, max_distance);
            float distance_z = transform.position.z + Random.Range(min_distance, max_distance);

            WP = Instantiate(wayPoint, new Vector3(distance_x,0.05f, distance_z), Quaternion.identity) as GameObject;

            wayP = true;
        }
    }

    private IEnumerator ChooseAction()
    {
        while (true)
        {
            yield return new WaitForSeconds(2.0f);
            if (!wayP)
            {
                int pickAction = Random.Range(0, 2);
                switch (pickAction)
                {
                    case 0:
                        movementState = MovementStates.IDLE;
                        break;
                    case 1:
                        movementState = MovementStates.WALKING;
                        break;
                    default:
                        break; 
                }
        
            }
        }
    }

    private void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, WP.transform.position, speed * Time.deltaTime);
        var rotation = Quaternion.LookRotation(WP.transform.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation,rotation,rotateSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "WayPoint")
        {
            Destroy(other.gameObject);
            wayP = false;
            movementState = MovementStates.IDLE;
        }
    }

}
