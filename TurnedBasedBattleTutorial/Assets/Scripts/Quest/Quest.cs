/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using System;

[Serializable]
public class Quest  {

	public enum QuestProgress
    {
        NOT_AVAILABLE,
        AVAILABLE,
        ACCEPTED,
        COMPLETE,
        DONE
    }

    public string title;            //title for quest(name of quest)
    public int id;                  //ID for quest
    public QuestProgress progress;  //Set the state of the quest
    public string description;      //quest details
    public string hint;
    public string congratulation;   //completed the quest message
    public string summary;
    public int nextQuest;           //next quest if there is any

    public string questObjective;   //name of quest objective
    public int questObjectiveCount; //current number of quest objectives
    public int questObjectiveRequirement; //required amount of quest objectives

    public int expReward;
    public int moneyReward;
    public string itemReward;


}
