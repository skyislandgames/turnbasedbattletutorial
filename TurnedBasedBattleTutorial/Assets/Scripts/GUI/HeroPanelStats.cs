/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using UnityEngine;
using UnityEngine.UI;

public class HeroPanelStats : MonoBehaviour {

    public Text HeroName;
    public Text HeroHP;
    public Text HeroMP;
    public Image ProgressBar;
	
}
