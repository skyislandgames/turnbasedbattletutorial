/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/



public class Fire1Spell : BaseAttack {
    public Fire1Spell()
    {
        attackName = "Fire 1";
        attackDescription = "A powerful fire spell";
        attackDamage = 25f;
        attactCost = 10f;
    }

}
