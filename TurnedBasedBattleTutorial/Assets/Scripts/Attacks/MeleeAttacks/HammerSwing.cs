/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using UnityEngine;

public class HammerSwing : BaseAttack {

    public HammerSwing()
    {
        attackName = "Hammer Swing";
        attackDescription = "A powerful hammer swing";
        attackDamage = 15f;
        attactCost = 0;
    }
}
