/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using UnityEngine;

public class Slash : BaseAttack {

    public Slash()
    {
        attackName = "Slash";
        attackDescription = "A fast slash attack";
        attackDamage = 10f;
        attactCost = 0;
    }

}
