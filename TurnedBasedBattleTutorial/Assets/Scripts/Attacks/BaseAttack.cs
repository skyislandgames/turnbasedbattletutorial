/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using System;
using UnityEngine;

[Serializable]
public class BaseAttack : MonoBehaviour {

    public string attackName;
    public string attackDescription;
    public float attackDamage;
    public float attactCost; //If special use MP
	
}
