/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using UnityEngine;

public class MagicAttack : MonoBehaviour {

    public BaseAttack magicAttackToPerform;

    public void CastMagicAttack()
    {
        GameObject.Find("BattleManager").GetComponent<BattleStateMachine>().Input4(magicAttackToPerform);
    }
}
