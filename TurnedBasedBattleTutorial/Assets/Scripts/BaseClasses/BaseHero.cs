/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using System;
using System.Collections.Generic;

[Serializable]
public class BaseHero : BaseClassAttributes {

    #region Variables

    public int stamina;
    public int intellect;
    public int dexterity;
    public int agility;

    public List<BaseAttack> MagicAttacks = new List<BaseAttack>();
	#endregion
	

	
}
