/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using System;
using System.Collections.Generic;

[Serializable]
public class BaseClassAttributes  {

    public string theName;

    public float curLevel;

    public float baseHP;
    public float curHP;

    public float baseMP;
    public float curMP;

    public float baseATK;
    public float curATK;

    public float baseDEF;
    public float curDEF;

    public List<BaseAttack> attacks = new List<BaseAttack>();
}
