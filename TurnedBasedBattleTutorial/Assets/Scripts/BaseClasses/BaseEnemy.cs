/*
* Copyright (c) Sky Island Games LLC
* www.skyislandgames.com
*/


using System;

[Serializable]
public class BaseEnemy :BaseClassAttributes {

    #region Variables


    public enum Type
    {
        GRASS,
        FIRE,
        WATER,
        ELECTRIC
    }

    public enum Rarity
    {
        COMMON,
        UNCOMMON,
        RARE,
        SUPERRARE
    }

    public Type EnemyType;
    public Rarity rarity;


    #endregion

}
